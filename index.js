let trainer = {
	name: 'Ash Ketchum',
	age: 10,
	pokemon: ['Pikachu','Charizard','Squirtle', 'Balbasaur'],
	friends: { 
		hoenn : ['May','Max'],
		kanto: ['Brock' , 'Misty']
	},
	talk: function(){
		console.log(this.pokemon[0] + "! I choose you");
	}
}

console.log(trainer)
console.log('Result of dot notation:')
console.log(trainer.name)
console.log('Result of square bracket notation:')
console.log(trainer['pokemon'])
console.log('Result of talk method')
trainer.talk();

function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	this.tackle = function(target){
		console.log(target.name + " tackled " + this.name);
		let newHealth = (this.health - target.attack)
		console.log(this.name + "'s health is reduced to " + (this.health - target.attack))
	
	 	if( newHealth <= 0){
	 		this.faint()
	 	}
	}
	this.faint = function(){
		console.log(this.name + ' fainted')
	}
}

let pikachu = new Pokemon("Pikachu", 30)
let glaceon = new Pokemon("Glaceon", 25)
let eevee = new Pokemon("Eevee", 15)
let turtwig = new Pokemon("Turtwig", 60)
console.log(pikachu)
console.log(glaceon)
console.log(eevee)
console.log(turtwig)

pikachu.tackle(glaceon)
eevee.tackle(turtwig)
